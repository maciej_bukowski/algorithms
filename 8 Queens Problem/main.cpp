#include <iostream>
using namespace std;

const int N = 8;

void show(int *t);
void clear(int *t);
void clear(int *t, int n);
bool test(int* t, int pos);
void findPositions(int *t, int pos);

int main() {
	int t[N];
	clear(t);
	findPositions(t, 0);

	system("pause");
	return 0;
}

void findPositions(int* t, int pos) {
	int counter = 0;
	while (true) {
		if (test(t, pos)) {
			if (pos == N - 1) {
				show(t);
				counter++;

				if (t[pos] + 1 < N) {
					t[pos]++;
				}
				else {
					t[pos] = 0;
					t[--pos]++;
				}
			}
			else {
				pos++;
			}
		}
		else {
			if (t[pos] + 1 < N) {
				t[pos]++;
			}
			else {
				if (pos == 0)
					break;
				t[pos] = 0;
				t[--pos]++;
			}
		}
	}

	cout << counter << endl;
}


bool test(int* t, int pos) {
	int a[N + N];

	// rows
	clear(a, N);
	for (int i = 0; i < pos + 1; i++) {
		if (a[t[i]])
			return false;
		a[t[i]] = 1;
	}

	//// diag a1 - h8
	clear(a, N + N);
	for (int i = 0; i < pos + 1; i++) {
		if (a[t[i] + i])
			return false;
		a[t[i] + i] = 1;
	}

	// diag a8-h1
	clear(a, N + N);
	for (int i = 0; i < pos + 1; i++) {
		if (a[N + t[i] - i])
			return false;
		a[N + t[i] - i] = 1;
	}

	return true;
}

void show(int *t) {
	for (int i = 0; i < N; i++)
		cout << t[i] << " ";
	cout << endl;
}

void clear(int *t) {
	for (int i = 0; i < N; i++)
		t[i] = 0;
}

void clear(int *t, int x) {
	for (int i = 0; i < x; i++)
		t[i] = 0;
}