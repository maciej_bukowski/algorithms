#include <iostream>
using namespace std;
typedef unsigned long long huge; // Alias

int* createSieve(int size) {
	int* sieve = new int[size];

	for (int i = 0; i < size; i++) {
		sieve[i] = 0;
	}

	for (int i = 2; i < size; i++) {
		if (sieve[i] == 1)
			continue;

		for (int j = 2; i*j < size; j++) {
			sieve[i*j] = 1;
		}
	}

	return sieve;
}

huge sumPrimesBelow(int size) {
	size++; // we have to create bigger array, because we start counting from 1

	if (size < 0)
		return 0;

	int* sieve = createSieve(size);

	huge sum = 0; // Result is greater than INT_MAX value
	for (int i = 2; i < size; i++) {
		
		if (sieve[i] == 0)
			sum += i;
	}
	return sum;
}


int main() {

	int size;

	cin >> size;
	cout << sumPrimesBelow(size) << endl;

	system("pause");
	return 0;
}