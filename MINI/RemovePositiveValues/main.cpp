#include <iostream>
using namespace std;

int countNumberOfNegativeNumbers(int* arr, int size) {
	int counter = 0;
	for (int i = 0; i < size; i++) {
		if (arr[i] < 0)
			counter++;
	}
	return counter;
}

int* removePositive(int* arr, int size) {

	int positiveNumbersIndex = countNumberOfNegativeNumbers(arr, size);
	int* newArr = new int[size];
	int negativeNumbersIndex = 0;

	for (int i = 0; i < size; i++) {
		if (arr[i] >= 0) {
			newArr[positiveNumbersIndex] = 0;
			positiveNumbersIndex++;
		}
		else {
			newArr[negativeNumbersIndex] = arr[i];
			negativeNumbersIndex++;
		}
	}
	return newArr;
}


int main() {
	int size;
	cin >> size;

	int *arr = new int[size];

	for (int i = 0; i < size; i++)
		cin >> arr[i];

	int* newArr = removePositive(arr, size);

	for (int i = 0; i < size; i++)
		cout << newArr[i] << "  ";

	system("pause");
	return 0;
}