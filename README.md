# README #

### Solved ###

* Knight's Tour (using Hamiltonian cycle problem)
* 8 Queens Problem (brute force)
* Remove Positive Values 
* Project Euler: 10

### To solve ###

* Cuboid and line collision
* Change-making problem

### Contributors ###

Feel free to ask or make pull request if you see a bug or want to add some great algorithm! :)
