#pragma once

class Point {
public:
	Point(int _x, int _y) {
		x = _x;
		y = _y;
	}
	int x;
	int y;
	bool isGood(int n, int m) {
		return x >= 0 && x < n && y >= 0 && y < m;
	}
};