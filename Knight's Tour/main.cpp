#include <iostream>
#include <chrono>
#include "functions.h"
#include "point.h"
using namespace std;
using namespace std::chrono;

long long getTime () {
	return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

int main() {
	long long m1 = getTime();

	int** board = findSequence(8, 8);
	show(board, 8, 8);

	cout << "Time: " << getTime() - m1 << "ms" << endl;

	system("pause");
	return 0;
}