#pragma once
#include "point.h"

int** createMatrix(int n, int m);
void show(int** matrix, int n, int m);
void clearArray(int* arr, int n);
int countAvailableFields(Point* p, int** board, int n, int m);
Point* findBest(Point* p, int** board, int n, int m);
int** findSequence(int n, int m);