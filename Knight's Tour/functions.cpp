#include <iostream>
#include "point.h"

using namespace std;

Point moveVectors[8] = {
	Point(1, 2),
	Point(2, 1),
	Point(2, -1),
	Point(1, -2),
	Point(-1, -2),
	Point(-2, -1),
	Point(-2, 1),
	Point(-1, 2)
};

int** createMatrix(int n, int m) {
	int **map = new int*[n];
	for (int i = 0; i < n; i++) {
		map[i] = new int[m];
		for (int j = 0; j < m; j++) {
			map[i][j] = 0;
		}
	}
	return map;
}

void show(int** matrix, int n, int m) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (matrix[i][j] < 10)
				cout << " ";
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void clearArray(int *arr, int n) {
	for (int i = 0; i < n; i++) {
		arr[i] = 0;
	}
}

int countAvailableFields(Point* p, int** board, int n, int m) {
	int sum = 0;
	for (int i = 0; i < 8; i++) {
		Point* v = &moveVectors[i];
		Point* p2 = new Point(v->x + p->x, v->y + p->y);

		if (p2->isGood(n, m) && board[p2->x][p2->y] == 0) {
			sum++;
		}
	}
	return sum;
}

Point* findBest(Point* p, int** board, int n, int m) {
	Point* bestPoint = new Point(0, 0);
	int minAvMoves = 10;

	for (int i = 0; i < 8; i++) {
		Point* v = &moveVectors[i];
		Point* p2 = new Point(v->x + p->x, v->y + p->y);

		if (p2->isGood(n, m) && board[p2->x][p2->y] == 0) {
			int avMoves = countAvailableFields(p2, board, n, m);
			if (avMoves < minAvMoves) {
				minAvMoves = avMoves;
				bestPoint = p2;
			}
		}
	}
	return bestPoint;
}

int** findSequence(int n, int m) {
	int** board = createMatrix(n, m);
	int *movesArray = new int[n*m];
	clearArray(movesArray, n*m);

	Point *pos = new Point(0, 0);
	board[0][0] = 1;
	int moves = 1;

	while (moves < n*m) {
		pos = findBest(pos, board, n, m);
		board[pos->x][pos->y] = moves;
		moves++;
		// show(board, 8, 8);
	}

	board[0][0] = 0;
	return board;
}