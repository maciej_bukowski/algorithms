#include <iostream>
#include "functions.h"
using namespace std;

void insert_sort(int *t, int n) {
	int i, j, temp;
	for (i = 1; i < n; i++)
	{
		temp = t[i];
		j = i;
		while (j > 0 && temp < t[j - 1]) {
			t[j] = t[j - 1];
			j--;
		}
		t[j] = temp;
	}
}

void bubble_sort(int *t, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n - i - 1; j++) {
			if (t[j] > t[j + 1]) {
				swap(t[j], t[j + 1]);
			}
		}
	}
}

void quick_sort(int *t, int left, int right) {
	if (left >= right)
		return;

	int p = left;
	for (int i = left + 1; i <= right; i++)
		if (t[left] > t[i]) {
			p++;
			swap(t[p], t[i]);
		}
	swap(t[left], t[p]);
	quick_sort(t, left, p - 1);
	quick_sort(t, p + 1, right);
}

int* generate_random(int n, int min, int max) {
	int *t = new int[n];
	for (int i = 0; i < n; i++)
		t[i] = rand() % (max - min) + min;
	return t;
}

void show(int *t, int n) {
	for (int i = 0; i < n; i++)
		cout << t[i] << " ";
	cout << endl;
}

int* copy(int *t, int n) {
	int *a = new int[n];
	for (int i = 0; i < n; i++)
		a[i] = t[i];
	return a;
}

bool compare(int ** t, int n, int m) {
	for (int i = 1; i < m; i++) {
		for (int j = 0; j < n; j++) {
			if (t[i][j] != t[0][j])
				return false;
		}
	}
	return true;
}
