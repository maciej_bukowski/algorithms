#include <iostream>
#include <ctime>
#include <cstdlib>
#include "functions.h"

using namespace std;

int main() {
	const int N = 10;
	srand((unsigned int) time(NULL));

	int *t1 = generate_random(N, 0, 100);
	int *t2 = copy(t1, N);
	int *t3 = copy(t1, N);

	int *matrix[3] = { t1, t2, t3 };
	
	show(t1, N);

	quick_sort(t1, 0, N-1);
	bubble_sort(t2, N);
	insert_sort(t3, N);

	show(t1, N);

	cout << "Success: " << compare(matrix, N, 3) << endl;

	delete t1, t2, t3;

	system("pause");
    return 0;
}