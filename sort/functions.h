void insert_sort(int *t, int n);
void bubble_sort(int *t, int n);
void quick_sort(int *t, int left, int right);
int* generate_random(int n, int min, int max);
void show(int *t, int n);
int* copy(int *t, int n);
bool compare(int **t, int n, int m);